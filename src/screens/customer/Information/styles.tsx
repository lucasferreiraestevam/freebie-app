import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  loading: {
    position: 'absolute',
    height: Height,
    width: Width,
    backgroundColor: '#FFF',
    elevation: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lottie: {
    height: Height * 0.12,
  },
  title: {
    position: 'relative',
    textAlign: 'center',
    marginTop: Height * 0.01,
    fontSize: Width * 0.045,
    color: '#072672',
  },
});

export default Styles;
