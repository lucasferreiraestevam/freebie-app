import React, {useState} from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import CardComponent from '../../../components/CardComponent';
import CustomerHeaderComponent from '../../../components/CustomerHeaderComponent';
import CustomerInformationComponent from '../../../components/CustomerInformationComponent';
import CustomerTitleComponent from '../../../components/CustomerTitleComponent';
import TabComponent from '../../../components/TabComponent';
import Styles from './styles';

function InformationScreen({navigation, route}: any) {
  const [title, setTitle] = useState('Itzza');
  const [location, setLocation] = useState('Itaim - SP');
  const [status, setStatus] = useState('Fechado');
  const [category, setCategory] = useState('Pizzaria');

  const pages = {
    0: {title: 'Ofertas', screen: 'Offers'},
    1: {title: 'Informações', screen: 'Information'},
  };

  const data = {
    0: {
      id: 'cd7acbea-c1b1-46c2-aed5-3ad53abb28bb',
      description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nunc eget lorem dolor sed viverra ipsum nunc aliquet. Urna neque viverra justo nec ultrices dui sapien eget. Ac turpis egestas sed tempus urna et pharetra. Senectus et netus et malesuada fames ac turpis egestas. Orci eu lobortis elementum nibh tellus molestie nunc non blandit. Ut lectus arcu bibendum at varius vel pharetra vel turpis.',
    },
  };

  return (
    <SafeAreaView>
      <View style={Styles.container}>
        <CustomerHeaderComponent
          banner="../assets/banner.png"
          logo="../assets/logo.png"
        />
        <View style={Styles.box}>
          <CustomerTitleComponent title={title} location={location} />
          <CustomerInformationComponent
            status={status}
            category={category}
            instagram=""
          />
          <TabComponent
            navigation={navigation}
            active={route.name}
            pages={pages}
          />
        </View>
        <CardComponent cards={data} />
      </View>
    </SafeAreaView>
  );
}

export default InformationScreen;
