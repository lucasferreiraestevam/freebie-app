import React, {useState} from 'react';
import {SafeAreaView, View, Text} from 'react-native';
import CustomerHeaderComponent from '../../../components/CustomerHeaderComponent';
import CustomerInformationComponent from '../../../components/CustomerInformationComponent';
import CustomerTitleComponent from '../../../components/CustomerTitleComponent';
import OfferRowComponent from '../../../components/OfferRowComponent';
import TabComponent from '../../../components/TabComponent';
import Styles from './styles';

function OffersScreen({navigation, route}: any) {
  const [title, setTitle] = useState('Itzza');
  const [location, setLocation] = useState('Itaim - SP');
  const [status, setStatus] = useState('Fechado');
  const [category, setCategory] = useState('Pizzaria');

  const pages = {
    0: {title: 'Ofertas', screen: 'Offers'},
    1: {title: 'Informações', screen: 'Information'},
  };

  const data = {
    0: {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: '10% Desconto',
      subTitle: 'Presencial',
    },
    1: {
      id: 'cd7acbea-c1b1-46c2-aed5-3ad53abb28bb',
      title: '30% Desconto',
      subTitle: 'Delivery',
    },
  };

  return (
    <SafeAreaView>
      <View style={Styles.container}>
        <CustomerHeaderComponent
          banner="../assets/banner.png"
          logo="../assets/logo.png"
        />
        <View style={Styles.box}>
          <CustomerTitleComponent title={title} location={location} />
          <CustomerInformationComponent
            status={status}
            category={category}
            instagram=""
          />
          <TabComponent
            navigation={navigation}
            active={route.name}
            pages={pages}
          />
        </View>
        <OfferRowComponent promotions={data} />
      </View>
    </SafeAreaView>
  );
}

export default OffersScreen;
