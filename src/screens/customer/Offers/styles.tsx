import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  container:{
    height: Height
  },
  box:{
    width: Width,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Height*0.08
  }
});

export default Styles;
