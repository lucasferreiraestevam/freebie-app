import React from 'react';
import {SafeAreaView, View, Text, Image, TouchableOpacity} from 'react-native';
import Styles from './styles';

export type Props = {
  pages: object;
  active: string;
  navigation: any;
};

const TabComponent: React.FC<Props> = ({pages, active, navigation}) => {
  return (
    <View style={Styles.box}>
      {Object.values(pages).map(i => {
        return (
          <TouchableOpacity onPress={() => navigation.navigate(`${i.screen}`)}>
            <Text
              style={
                active == i.screen
                  ? Styles.tabTitleActive
                  : Styles.tabTitleInactive
              }>
              {i.title}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default TabComponent;
