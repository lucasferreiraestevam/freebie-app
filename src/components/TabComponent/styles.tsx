import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  box: {
    width: Width,
    marginTop: Width / 15,
    paddingLeft: Width / 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  tabTitleInactive: {
    fontSize: Width * 0.036,
    fontWeight: '500',
    marginRight: Width / 15,
    color: '#1D1D1D',
  },
  tabTitleActive: {
    fontSize: Width * 0.036,
    fontWeight: '500',
    marginRight: Width / 15,
    color: '#523C77',
  },
});

export default Styles;
