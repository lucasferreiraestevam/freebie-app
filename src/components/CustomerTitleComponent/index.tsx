import React from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import Styles from './styles';

export type Props = {
  title: string;
  location: string;
};

const CustomerTitleComponent: React.FC<Props> = ({title, location}) => {
  return (
    <View>
      <View>
        <Text style={Styles.title}>{title}</Text>
      </View>
      <View>
        <Text style={Styles.location}>{location}</Text>
      </View>
    </View>
  );
};

export default CustomerTitleComponent;
