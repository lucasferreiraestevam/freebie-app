import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  title: {
    fontSize: Height * 0.02,
    fontWeight: '600',
    color: '#523C77',
    textAlign: 'center',
  },
  location: {
    fontSize: Height * 0.02,
    fontWeight: '500',
    color: '#828282',
    textAlign: 'center',
  },
});

export default Styles;
