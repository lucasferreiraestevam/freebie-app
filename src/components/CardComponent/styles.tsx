import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  box: {
    display: 'flex',
    padding: Width / 15,
  },
  row: {
    display: 'flex',
    flexDirection: 'column',
    width: Width * 0.87,
    padding: Width / 15,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    marginBottom: Height * 0.03,
  },
  textBox: {
    flexDirection: 'row',
    marginBottom: Height * 0.02
  },
  imageBox: {
    width: '40%',
    height: '100%',
  },
  beeIcon: {
    top: -20,
  },
  rowTitle: {
    marginLeft: Width * 0.02,
    fontSize: Width * 0.043,
    fontWeight: '600',
    color: '#523C77',
  },
  rowDescription: {
    fontSize: Width * 0.036,
    marginBottom: 5,
    fontWeight: '400',
    textAlign: 'justify',
    color: '#1D1D1D',
  },
  rowSubTitle: {
    fontWeight: '400',
  },
});

export default Styles;
