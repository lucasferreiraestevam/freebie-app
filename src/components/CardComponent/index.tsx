import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Styles from './styles';
import InformationVector from '../../assets/info_circle.svg';

export type Props = {
  cards: object;
};

const renderItem = ({item}: any) => {
  return (
    <TouchableOpacity onPress={() => console.warn(item.title)}>
      <View style={Styles.row}>
      <View style={Styles.textBox}>
        <InformationVector/>
        <Text style={Styles.rowTitle}>Descrição</Text>
      </View>
        <View style={Styles.textBox}>
          <Text style={Styles.rowDescription}>{item.description}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const CardComponent: React.FC<Props> = ({cards}) => {
  return (
    <View style={Styles.box}>
      <FlatList
        data={Object.values(cards)}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

export default CardComponent;
