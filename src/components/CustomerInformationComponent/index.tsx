import React from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import Styles from './styles';
import ClockVector from '../../assets/clock.svg';
import InstagramVector from '../../assets/instagram.svg';

export type Props = {
  status: string;
  category: string;
  instagram: string;
};

const CustomerInformationComponent: React.FC<Props> = ({
  status,
  category,
  instagram,
}) => {
  return (
    <View style={Styles.box}>
      <View style={Styles.statusButton}>
        <ClockVector style={Styles.clockIcon} />
        <Text style={Styles.statusTitle}>{status}</Text>
      </View>
      <View style={Styles.categoryButton}>
        <Text style={Styles.categoryTitle}>{category}</Text>
      </View>
      <View style={Styles.instagramContainer}>
        <InstagramVector style={Styles.instagramIcon} />
      </View>
    </View>
  );
};

export default CustomerInformationComponent;
