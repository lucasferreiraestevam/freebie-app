import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  box: {
    width: Width,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  statusButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#FF5B4A',
    width: Width * 0.22,
    height: Width * 0.07,
    borderRadius: 100,
    margin: 10,
  },
  statusTitle: {
    textAlign: 'center',
    color: '#FFF',
    fontWeight: '500',
  },
  categoryButton: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    backgroundColor: '#EBEBEB',
    width: Width * 0.22,
    height: Width * 0.07,
    borderRadius: 100,
    margin: 10,
  },
  categoryTitle: {
    textAlign: 'center',
    color: '#1D1D1D',
    fontWeight: '500',
  },
  instagramContainer: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    margin: 10,
  },
  instagramIcon:{
    width: 10,
    height: 10
  },
  clockIcon:{
    width: 10,
    height: 10,
    marginRight: 2
  }
});

export default Styles;
