import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  box: {
    display: 'flex',
    padding: Width / 15,
  },
  row: {
    display: 'flex',
    flexDirection: 'row',
    width: Width * 0.87,
    height: Height * 0.088,
    padding: Width / 15,
    backgroundColor: 'rgba(222, 222, 222, 0.5)',
    borderRadius: 5,
    marginBottom: Height * 0.03,
  },
  textBox: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    width: '60%',
    height: '100%',
  },
  imageBox: {
    width: '40%',
    height: '100%',
  },
  beeIcon: {
    top: -20,
  },
  rowTitle: {
    fontSize: Width * 0.036,
    marginBottom: 5,
    fontWeight: '500',
  },
  rowSubTitle: {
    fontWeight: '400',
  },
});

export default Styles;
