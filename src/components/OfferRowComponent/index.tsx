import React from 'react';
import {
  SafeAreaView,
  View,
  FlatList,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import Styles from './styles';

export type Props = {
  promotions: object;
};

const renderItem = ({item}: any) => {
  return (
    <TouchableOpacity onPress={() => console.warn(item.title)}>
      <View style={Styles.row}>
        <View style={Styles.textBox}>
          <Text style={Styles.rowTitle}>{item.title}</Text>
          <Text style={Styles.rowSubTitle}>{item.subTitle}</Text>
        </View>
        <View style={Styles.imageBox}>
          <Image source={require('../../assets/bee.png')} style={Styles.beeIcon} />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const OfferRowComponent: React.FC<Props> = ({promotions}) => {
  return (
    <View style={Styles.box}>
      <FlatList
        data={Object.values(promotions)}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

export default OfferRowComponent;
