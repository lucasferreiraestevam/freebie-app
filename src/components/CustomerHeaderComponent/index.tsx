import React from 'react';
import {SafeAreaView, View, Text, Image} from 'react-native';
import Styles from './styles';

export type Props = {
  banner: string;
  logo: string;
};

const CustomerHeaderComponent: React.FC<Props> = ({banner, logo}) => {
  return (
    <View>
      <Image
        style={Styles.banner}
        source={require('../../assets/banner.png')}
      />
      <View style={Styles.logoWrapper}>
        <View style={Styles.logoContainer}>
          <Image
            style={Styles.logo}
            source={require('../../assets/logo.png')}
          />
        </View>
      </View>
    </View>
  );
};

export default CustomerHeaderComponent;
