import {StyleSheet, Dimensions} from 'react-native';

const Width = Dimensions.get('window').width;
const Height = Dimensions.get('window').height;

const Styles = StyleSheet.create({
  banner: {
    width: Width,
  },
  logoWrapper: {
    width: Width,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoContainer: {
    position: 'absolute',
    top: -Height / 15,
    width: Height * 0.13,
    height: Height * 0.13,
  },
  logo: {
    width: '100%',
    height: '100%',
  },
});

export default Styles;
