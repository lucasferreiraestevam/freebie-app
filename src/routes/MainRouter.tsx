import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import CustomerStack from './CustomerStack';

const Stack = createNativeStackNavigator();

const MainRouter = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="HomeStack"
        screenOptions={{headerBackTitleVisible: false, header: () => null}}>
        {/* Stacks */}
        <Stack.Screen name="CustomerStack" component={CustomerStack} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default MainRouter;
