import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import OffersScreen from '../screens/customer/Offers';
import InformationScreen from '../screens/customer/Information';

const Stack = createNativeStackNavigator();

function CustomerStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Offers" component={OffersScreen} />
      <Stack.Screen name="Information" component={InformationScreen} />
    </Stack.Navigator>
  );
}

export default CustomerStack;
