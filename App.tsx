import React from 'react';
import MainRouter from './src/routes/MainRouter';

const App = () => {
  return <MainRouter />;
};

export default App;
